import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SlmpleFoodOrderingMachine2 {

    public static int i = 0;

    void order(String food, int m) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering! " + food + " It will be received as soon as possible.");
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food + " " + m + "yen" + "\n");
            i += m;
            abc.setText("Total  " + i + "yen");

        }

    }

    private JPanel root;
    private JButton button1;
    private JButton frenchFries140yenButton;
    private JButton chickennugget150yenButton;
    private JButton coke100yenButton;
    private JButton iceCream120yenButton;
    private JButton salad130yenButton;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel abc;

    public static void main(String[] args) {
        JFrame frame = new JFrame("SlmpleFoodOrderingMachine2");
        frame.setContentPane(new SlmpleFoodOrderingMachine2().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    public SlmpleFoodOrderingMachine2() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburger", 160);
            }
        });
        button1.setIcon(new ImageIcon(this.getClass().getResource("hamber.png")
        ));

        frenchFries140yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("French fries", 140);
            }
        });
        frenchFries140yenButton.setIcon(new ImageIcon(this.getClass().getResource("poteto.png")
        ));

        chickennugget150yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chickennugget", 150);
            }
        });
        chickennugget150yenButton.setIcon(new ImageIcon(this.getClass().getResource("chiken .png")
        ));

        salad130yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salad", 130);
            }
        });
        salad130yenButton.setIcon(new ImageIcon(this.getClass().getResource("sarada.png")
        ));

        iceCream120yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ice cream", 120);
            }
        });
        iceCream120yenButton.setIcon(new ImageIcon(this.getClass().getResource("ice.png")
        ));

        coke100yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coke", 100);
            }
        });
        coke100yenButton.setIcon(new ImageIcon(this.getClass().getResource("cokea.png")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout? ",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    textPane1.setText(null);
                    JOptionPane.showMessageDialog(null, "Thank you .The total price is "
                            + i + " yen.");
                    i = 0;
                    abc.setText("Total  " + i + " yen");
                }
            }
        });
    }

}

